# some of my dot files

## .rpmmacros

see also https://wiki.centos.org/HowTos/SetupRpmBuildEnvironment

## .inputrc

- up/down arrow to search the history matching what is already typed

## .screenrc (minimal)

- I don't remember where I found it :( to acknowlede the credits

## .config/Code/User/settings.json

- disable telemetry for [Visual Studio Code](https://code.visualstudio.com/)
as reported in the [FAQ as of 2017/07/26](https://code.visualstudio.com/docs/supporting/faq)
